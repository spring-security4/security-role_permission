package com.example.springsecurityrolepermission.securityconfig;

import java.util.Set;
import com.google.common.collect.Sets;

import static com.example.springsecurityrolepermission.securityconfig.UserPermission.*;

/**
 * Sets.hewHashSet i istifade ede bilmek ucun google un guava dependency sini istifade edirik
 */
public enum UserRole {
    CUSTOMER(Sets.newHashSet()),
    ADMIN(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,EMPLOYEE_READ,EMPLOYEE_WRITE)),
    MANAGER(Sets.newHashSet(CUSTOMER_READ,EMPLOYEE_READ));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }
}
