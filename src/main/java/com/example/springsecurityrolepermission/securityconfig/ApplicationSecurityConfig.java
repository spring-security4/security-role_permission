package com.example.springsecurityrolepermission.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.springsecurityrolepermission.securityconfig.UserRole.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     *  Yazmis oldugumuz enum lardaki role ve permission lara gore
     *  customer leri ("/api/**").hasRole(CUSTOMER.name())  role Customer e verdiyimiz ucun bizdede yalniz lale123 ile olan username
     *  bele role a sahibdi ve buna gore kodu ise saldigimizda bu metodlari yalniz lale123 username i getire bilir
     *  tebii ki heleki mentiq duzgun qurulmuyub sadece role lara gore nece isdiyir buna baxiriq
     *  biz el123 username ile daxil olmaga calissaq bize 403 Forbidden exceptioni atacaq
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .antMatchers("/api/**").hasRole(CUSTOMER.name()) // api/**  yeni api/v1/customers/  , her ulduz '/' dan sonraki hisseni gosterir
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .roles(ADMIN.name())
                .build();

        UserDetails ilqar = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("ilqar123")
                .password(passwordEncoder.encode("1234"))
                .roles(MANAGER.name())
                .build();

        UserDetails lale = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("lale123")
                .password(passwordEncoder.encode("1234"))
                .roles(CUSTOMER.name())
                .build();

        return new InMemoryUserDetailsManager(
                elchin,
                ilqar,
                lale
        );
    }
}
