package com.example.springsecurityrolepermission.controller;

import com.example.springsecurityrolepermission.model.Customer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/customers/")
public class CustomerController {

    @GetMapping("{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") Long customerId){
        return CUSTOMERS.stream()
                .filter(customer -> customerId.equals(customer.getId()))
                .findFirst()
                .orElseThrow(()-> new IllegalStateException("Customer " + customerId + " does not exists"));
    }

    @GetMapping
    public List<Customer> getAllCustomer(){
        return CUSTOMERS;
    }

    private static final List<Customer> CUSTOMERS = Arrays.asList(
            new Customer(1L,"Elchin","Akbarov", UUID.randomUUID().toString()),
            new Customer(2L,"Nigar","Rehimova", UUID.randomUUID().toString()),
            new Customer(3L,"Saleh","Asgarov", UUID.randomUUID().toString()),
            new Customer(4L,"Nicat","Bayramov", UUID.randomUUID().toString()),
            new Customer(5L,"Leman","Musayeva", UUID.randomUUID().toString())
    );
}
