package com.example.springsecurityrolepermission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRolePermission {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityRolePermission.class, args);
    }

}
